FROM python:3.8.10
LABEL MAINTAINER="Sukhbat Amartugs"

ENV PYTHONBUFFERED 1
# RUN apk add libaio libnsl libc6-compat && \
#       ln -s /lib64/* /lib 
# RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories && \
#       apk add --update libaio libnsl && \
#       ls -ltr /usr/lib/libc* && \
#       ln -s /usr/lib/libnsl.so.3 /usr/lib/libnsl.so.1 && \
#       ln -s /lib/libc.so.6 /usr/lib/libresolv.so.2
# RUN mkdir /etc/ld.so.conf.d    

# # Install dependencies
# COPY ./requirements.txt /requirements.txt
# RUN apk add --update --no-cache postgresql-client
# RUN apk add --update --no-cache --virtual .tmp-build-deps \
#       gcc libc-dev freetype-dev libffi-dev linux-headers postgresql-dev \
#       tiff-dev jpeg-dev openjpeg-dev zlib-dev freetype-dev lcms2-dev \
#       libwebp-dev tcl-dev tk-dev harfbuzz-dev fribidi-dev libimagequant-dev \
#       libxcb-dev libpng-dev
# RUN apk del .tmp-build-deps

# RUN apk add busybox-extras
RUN apt-get update && apt-get install -y python3-opencv
RUN apt-get -y install tesseract-ocr \ 
  && apt-get -y install ffmpeg libsm6 libxext6
COPY ./requirements.txt /requirements.txt
RUN --mount=type=cache,target=/root/.cache \
    python3 -m pip install -r /requirements.txt

RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list 
# RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | tee /usr/share/keyrings/cloud.google.gpg 
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
RUN apt-get update -y && apt-get install google-cloud-sdk -y
RUN mkdir -p /root/.config/gcloud
RUN touch /root/.config/gcloud/application_default_credentials.json
COPY ./hipay-loyalty-3e1d1-759dabba7312.json /root/.config/gcloud/application_default_credentials.json
# RUN gcloud auth application-default login
RUN gcloud auth activate-service-account cloud-vision@hipay-loyalty-3e1d1.iam.gserviceaccount.com --key-file=/root/.config/gcloud/application_default_credentials.json
ENV GOOGLE_APPLICATION_CREDENTIALS=/root/.config/gcloud/application_default_credentials.json
RUN gcloud config set project hipay-loyalty-3e1d1

RUN mkdir app
WORKDIR /app
COPY ./ /app
RUN mkdir urlImages
RUN mkdir /mnt/imgc
EXPOSE 8000
# RUN adduser -D user
# USER user
