import os
import requests
from datetime import datetime

def download_file(url, folder_path, filename=None):
    """
    Download a file from a URL and save it to a specified folder with an optional new filename.

    :param url: URL of the file to download
    :param folder_path: Path to the folder where the file should be saved
    :param filename: New name of the file. If None, uses the last segment of the URL.
    """
    if filename is None:
        filename = url.split('/')[-1]
    
    # Ensure the folder exists
    os.makedirs(folder_path, exist_ok=True)
    
    # Full path to save the file
    file_path = os.path.join(folder_path, filename)

    # Download the file
    response = requests.get(url)
    response.raise_for_status()  # Check if the download was successful

    # Write the file to the specified folder
    with open(file_path, 'wb') as f:
        f.write(response.content)

    print(f"File downloaded and saved to: {file_path}")
