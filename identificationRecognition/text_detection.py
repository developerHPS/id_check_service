import time
import re
from difflib import SequenceMatcher
import requests
import json
from google.cloud import vision
import io
import validators
from mybend import settings
import cv2, face_recognition


def checkRegno(text):
    regexRegno = r"^(([А-Я|Ө|Ү]{2})(\d{8}))|(\d{12})$"
    regex = re.compile(regexRegno)
    if regex.match(text):
        return True
    else:
        return False


from mybend import settings


def checkAllMongolian(text):
    regexAllMongolian = r"^[а-яА-Я|Ө|Ү|ө|ү|-]+$"
    regex = re.compile(regexAllMongolian)
    if regex.match(text):
        return True
    else:
        return False


def checkDate(date_string):
    date_regex = re.compile(r"^\d{4}/\d{2}/\d{2}$")
    if date_regex.match(date_string):
        return True
    else:
        return False


def checkNearWordExists(string1, string2, ratio):
    s = SequenceMatcher(None, string1, string2)
    similarity_ratio = s.ratio()
    # Check if the similarity is above 70%
    if similarity_ratio >= ratio:
        return True
    else:
        return False


def has_numbers(inputString):
    return any(char.isdigit() for char in inputString)


def has_cyrillic(text):
    return bool(re.search("[а-яА-Я|Ө|Ү|ө|ү]", text))


def detect_text_uri(uri):
    client = vision.ImageAnnotatorClient()
    image = vision.Image()
    image.source.image_uri = uri

    response = client.text_detection(image=image)

    if response.error.message:
        raise Exception("{}".format(response.error.message))
    texts = response.text_annotations
    if len(texts) > 0:
        return texts[0].description
    return ""


def detect_text(path):
    """Detects text in the file."""

    client = vision.ImageAnnotatorClient()

    with io.open(path, "rb") as image_file:
        content = image_file.read()

    image = vision.Image(content=content)

    response = client.text_detection(image=image)
    if response.error.message:
        raise Exception("{}".format(response.error.message))
    texts = response.text_annotations
    if len(texts) > 0:
        print(texts[0].description)
        return texts[0].description
    return ""


def passwordFrontLocal(texts):
    surname = None
    firstname = None
    lastname = None
    birthdate = None
    regno = None
    gender = None
    it = 0
    while it < len(texts):
        current_str = texts[it].strip()
        if "эцэг(эх)" in current_str.lower() or "surname" in texts[it].lower().strip():
            it += 1
            if checkAllMongolian(texts[it].lower().strip()):
                lastname = texts[it].lower().strip()
            else:
                print("lastname not all mongolian: " + texts[it].lower().strip())
        elif "given name" in current_str.lower():
            it += 1
            if checkAllMongolian(texts[it].lower().strip()):
                firstname = texts[it].lower().strip()
            else:
                print("firstname not all mongolian: " + texts[it].lower().strip())
        elif 'sex' in current_str.lower() or 'хүйс' in current_str.lower():
            it += 1
            if 'f' in texts[it].lower().strip():
                gender = 'female'
            else:
                gender = 'male'
        elif "date of birth" in current_str.lower() or "төрсөн он" in current_str.lower():
            it += 1
            found = False
            for i in range(it, len(texts)):
                if checkDate(texts[i].lower().strip().replace('\\', '/').replace('-', '/').replace('.', '/')):
                    birthdate = texts[i].lower().strip()
                    found = True
                    break
            if found == False:
                print("date is not correct: " + texts[it].lower().strip())
        elif (
                "personal no" in texts[it].lower().strip()
                or "бүртгэлийн" in texts[it].lower().strip()
        ):
            it += 1
            current_str = texts[it].lower().strip()
            regnoArr = current_str.split("/")
            for tmp in regnoArr:
                tmp = tmp.strip()
                print(tmp)
                first_two = tmp[:2].replace("0", "о").replace("x", "х")
                last_eight = (
                    tmp[2:10]
                        .replace("о", "0")
                        .replace("в", "8")
                        .replace("b", "8")
                        .replace("э", "3")
                        .replace("з", "3")
                )
                print(first_two + last_eight)
                if checkRegno((first_two + last_eight).upper()):
                    regno = (first_two + last_eight).upper()
                    break
        it += 1
    if surname or lastname or firstname or birthdate or regno or gender:
        url = str(settings.STS_API_URL) + "profile/set_from_idcard"
        body_dict = {}
        if surname is not None:
            body_dict['surname'] = surname
        if lastname is not None:
            body_dict['lastname'] = lastname
        if firstname is not None:
            body_dict['firstname'] = firstname
        if birthdate is not None:
            body_dict['birthday'] = birthdate.replace('\\', '-').replace('/', '-').replace('.', '-').replace(',', '-')
        if regno is not None:
            body_dict['regno'] = regno
        if gender is not None:
            body_dict['gender'] = gender
        return body_dict
    return {}
def mainFrontLocal(texts):
    surname = None
    firstname = None
    lastname = None
    birthdate = None
    regno = None
    gender = None
    it = 0
    while it < len(texts):
        current_str = texts[it].strip()
        if "family name" in current_str.lower():
            it += 1
            if checkAllMongolian(texts[it].lower().strip()):
                surname = texts[it].lower().strip()
            else:
                print("surname not all mongolian: " + texts[it].lower().strip())
        elif "эцэг/эх" in current_str.lower() or "surname" in texts[it].lower().strip():
            it += 1
            if checkAllMongolian(texts[it].lower().strip()):
                lastname = texts[it].lower().strip()
            else:
                print("lastname not all mongolian: " + texts[it].lower().strip())
        elif "given name" in current_str.lower():
            it += 1
            if checkAllMongolian(texts[it].lower().strip()):
                firstname = texts[it].lower().strip()
            else:
                print("firstname not all mongolian: " + texts[it].lower().strip())
        elif 'sex' in current_str.lower() or 'хүйс' in current_str.lower():
            it += 1
            if 'female' in texts[it].lower().strip():
                gender = 'female'
            else:
                gender = 'male'
        elif "birth" in current_str.lower() or "date" in current_str.lower():
            it += 1
            found = False
            for i in range(it, len(texts)):
                if checkDate(texts[i].lower().strip().replace('\\', '/').replace('-', '/').replace('.', '/')):
                    birthdate = texts[i].lower().strip()
                    found = True
                    break
            if found == False:
                print("date is not correct: " + texts[it].lower().strip())
        elif (
                "регистр" in texts[it].lower().strip()
                or "registr" in texts[it].lower().strip()
        ):
            it += 1
            current_str = texts[it].lower().strip()
            regnoArr = current_str.split("/")
            for tmp in regnoArr:
                tmp = tmp.strip()
                print(tmp)
                first_two = tmp[:2].replace("0", "о").replace("x", "х")
                last_eight = (
                    tmp[2:10]
                        .replace("о", "0")
                        .replace("в", "8")
                        .replace("b", "8")
                        .replace("э", "3")
                        .replace("з", "3")
                )
                print(first_two + last_eight)
                if checkRegno((first_two + last_eight).upper()):
                    regno = (first_two + last_eight).upper()
                    break
        elif (
                "civil" in texts[it].lower().strip()
                or "identification" in texts[it].lower().strip()
        ):
            it += 1
            current_str = texts[it].lower().strip()
            regnoArr = current_str.split("/")
            if checkRegno((current_str).upper()):
                regno = current_str
                break

        it += 1
    if surname or lastname or firstname or birthdate or regno or gender:
        url = str(settings.STS_API_URL) + "profile/set_from_idcard"
        body_dict = {}
        if surname is not None:
            body_dict['surname'] = surname
        if lastname is not None:
            body_dict['lastname'] = lastname
        if firstname is not None:
            body_dict['firstname'] = firstname
        if birthdate is not None:
            body_dict['birthday'] = birthdate.replace('\\', '-').replace('/', '-').replace('.', '-').replace(',', '-')
        if regno is not None:
            body_dict['regno'] = regno
        if gender is not None:
            body_dict['gender'] = gender
        return body_dict
    return {}


def mainBackLocal(texts):
    it = 0
    expiryDate = None
    while it < len(texts):
        current_str = texts[it].strip()
        if "date of issue" in current_str.lower():
            it += 1
            if checkDate(texts[it].lower().strip()):
                issueDate = texts[it].lower().strip()
            else:
                print("date is not correct: " + texts[it].lower().strip())
        elif "date of expiry" in current_str.lower():
            it += 1
            if checkDate(texts[it].lower().strip()):
                expiryDate = texts[it].lower().strip()
            else:
                print("date is not correct: " + texts[it].lower().strip())
        it += 1
    body_dict = {}
    if expiryDate is not None:
        body_dict = {"idcard_expiry": expiryDate.replace('\\', '-').replace('/', '-').replace('\\', '-').replace('/',
                                                                                                                 '-').replace(
            '.', '-').replace(',', '-')}
    return body_dict


def mainFrontGoogle(texts, customerId):
    body_dict = mainFrontLocal(texts)
    if body_dict != {}:
        body_dict['customerId'] = customerId
        payload = json.dumps(body_dict)
        headers = {
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(payload)
        print(response.text)


def mainBackGoogle(texts, customerId):
    starttime = time.time()
    body_dict = mainBackLocal(texts)
    body_dict['customerId'] = customerId
    if 'idcard_expiry' in body_dict and body_dict['idcard_expiry'] is not None:
        url = str(settings.STS_API_URL) + "profile/set_from_idcard"
        payload = json.dumps(body_dict)
        headers = {
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(payload)
        print(response.text)


def mainPasswordOcrDetect(path):
    checkStringMn = "ПАСПОРТ"
    checkStringEng = "PASSPORT"
    valid = validators.url(path)
    if valid == True:
        text = detect_text_uri(path)
    else:
        text = detect_text(path)
    texts = text.split("\n")
    print(texts)
    flag = 0
    if len(texts) > 5:
        for t in texts:
            if checkStringMn.lower() in t.lower().strip() or checkStringEng.lower() in t.lower().strip():
                flag = 1
                break
    if flag == 0:
        raise Exception("Error no detection")
    else:
        return passwordFrontLocal(texts)


def mainBothOcrDetect(path, id_type):
    if id_type == 1:
        checkStringMn = "МОНГОЛ УЛСЫН ИРГЭН"
        checkStringEng = "CITIZEN IDENTITY CARD"
    else:
        checkStringMn = "Улсын бүртгэлийн Ерөнхий газар"
        checkStringEng = "The General Authority for State Registration"
    valid = validators.url(path)
    if valid == True:
        text = detect_text_uri(path)
    else:
        text = detect_text(path)
    texts = text.split("\n")
    print(texts)
    flag = 0

    if len(texts) > 5:
        for t in texts:
            if checkStringMn.lower() in t.lower().strip() or checkStringEng.lower() in t.lower().strip():
                flag = 1
                break
    if flag == 0:
        raise Exception("Error no detection")
    else:
        if id_type == 1:
            return mainFrontLocal(texts)
        else:
            return mainBackLocal(texts)


def find_face_encodings(image_path):
    # reading image
    image = cv2.imread(image_path)
    # get face encodings from the image
    face_enc = face_recognition.face_encodings(image)
    # return face encodings
    return face_enc[0]


def checkFaceMatch(path1, path2):
    image_1 = find_face_encodings(path1)
    image_2 = find_face_encodings(path2)

    is_same = face_recognition.compare_faces([image_1], image_2)[0]
    print(f"Is Same: {is_same}")
    return is_same


def mainBothPath(path, customerId, id_type=0):
    starttime = time.time()
    checkStringFrontMn = "МОНГОЛ УЛСЫН ИРГЭН"
    checkStringFrontEng = "CITIZEN IDENTITY CARD"
    checkStringBackMn = "Улсын бүртгэлийн Ерөнхий газар"
    checkStringBackEng = "The General Authority for State Registration"
    valid = validators.url(path)
    if valid == True:
        text = detect_text_uri(path)
    else:
        text = detect_text(path)
    texts = text.split("\n")

    print(texts)
    flag = 0
    if len(texts) >= 5:
        for t in texts:
            if checkStringBackMn.lower() in t.lower().strip() or checkStringBackEng.lower() in t.lower().strip():
                flag = 2
                break
    if flag == 0 and len(texts) > 5:
        for t in texts:
            if checkStringFrontMn.lower() in t.lower().strip() or checkStringFrontEng.lower() in t.lower().strip():
                flag = 1
                break

    if flag == 0:
        raise Exception("ERROr no detection")
    elif flag == 1:
        mainFrontGoogle(texts, customerId)
    elif flag == 2:
        mainBackGoogle(texts, customerId)
    endtime = time.time()
    print("Duration: " + str(endtime - starttime))

# mainFrontGoogle('/home/sukhee/Documents/zurag2/880b0613-67fc-4e80-a966-58c29e647d27.jpeg')
# mainFrontGoogle('/home/sukhee/Documents/zurag2/e9f46b7f-85cf-44cc-aa34-c0368844c385.jpeg')
# mainFrontGoogle('/home/sukhee/Documents/zurag2/e9f46b7f-85cf-44cc-aa34-c0368844c385-1.jpeg')
# mainFrontGoogle(
#     "/home/sukhee/Documents/zurag2/bf7593fa-6931-499c-9086-ca433468cd34.jpeg"
# )
# mainBackGoogle(
#     "/home/sukhee/Documents/zurag2/0b057535-8ae6-4cb2-8b00-d02daa6b6b3e.jpeg"
# )
