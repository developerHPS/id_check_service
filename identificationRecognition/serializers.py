from pyrsistent import field
from rest_framework import serializers
from .models import IdentificationRecognition
from .models import IdentificationList
from .models import IdentificationUrlOrName
from .models import GetIdentificationData


class IdentificationRecognitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = IdentificationRecognition
        fields = ['template', 'idImage']

class IdentificationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = IdentificationList
        fields = ['idList']

class IdentificationUrlOrNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = IdentificationUrlOrName
        fields = ['urlOrName', 'customerId']

class IdentificationCustomerIdSerializer(serializers.Serializer):
    customerId = serializers.CharField(max_length=36)
    urlOrName = serializers.CharField(max_length=150)

class IdentificationOcrSerializer(serializers.Serializer):
    urlOrName = serializers.CharField(max_length=150)
    idType = serializers.ChoiceField(choices = (("1", "front"), ("2", "back")))

class IdentificationOcrPassportSerializer(serializers.Serializer):
    urlOrName = serializers.CharField(max_length=150)

class IdentificationFaceMatchSerializer(serializers.Serializer):
    urlOrName1 = serializers.CharField(max_length=150)
    urlOrName2 = serializers.CharField(max_length=150)

class Get_IdentificationDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = GetIdentificationData
        fields = ['urlOrName']
        