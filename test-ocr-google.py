
import time
#import easyocr
import re
from difflib import SequenceMatcher


def checkRegno(text):
	regexRegno = r"^(([А-Я|Ө|Ү]{2})(\d{8}))|(\d{12})$"
	regex = re.compile(regexRegno)
	if regex.match(text):
		return True
	else:
		return False

def checkAllMongolian(text):
	regexAllMongolian = r"^[а-яА-Я|Ө|Ү|ө|ү|-]+$"
	regex = re.compile(regexAllMongolian)
	if regex.match(text):
		return True
	else:
		return False

def checkDate(date_string):
	date_regex = re.compile(r'^\d{4}/\d{2}/\d{2}$')

	if date_regex.match(date_string):
	    return True
	else:
	    return False

def checkNearWordExists(string1, string2, ratio):
	s = SequenceMatcher(None, string1, string2)
	similarity_ratio = s.ratio()

	# Check if the similarity is above 70%
	if similarity_ratio >= ratio:
	    return True
	else:
	    return False

def has_numbers(inputString):
    return any(char.isdigit() for char in inputString)

def has_cyrillic(text):
    return bool(re.search('[а-яА-Я|Ө|Ү|ө|ү]', text))

def detect_text_uri(uri):
    """Detects text in the file located in Google Cloud Storage or on the Web.
    """
    from google.cloud import vision
    client = vision.ImageAnnotatorClient()
    image = vision.Image()
    image.source.image_uri = uri

    response = client.text_detection(image=image)
    texts = response.text_annotations
    if len(texts) > 0:
    	return texts[0].description

    if response.error.message:
        raise Exception(
            '{}'.format(
                response.error.message))
    return ''

def detect_text(path):
    """Detects text in the file."""
    from google.cloud import vision
    import io
    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.Image(content=content)

    response = client.text_detection(image=image)
    texts = response.text_annotations

    if response.error.message:
        raise Exception(
            '{}'.format(
                response.error.message))
    # print(texts[0].description)
    return texts[0].description

def mainFrontGoogle(path):
	starttime = time.time()
	checkStringFrontMn = 'МОНГОЛ УЛСЫН ИРГЭНИЙ ҮНЭМЛЭХ';
	checkStringFrontEng = 'CITIZEN IDENTITY CARD OF MONGOLIA';
	text = detect_text_uri(path)
	# print(text)
	texts = text.split('\n')
	if len(texts) < 6 or (texts[0] != checkStringFrontMn and texts[1] != checkStringFrontEng) :
		return 'not id card FRONT'
	surname = None
	firstname = None
	lastname = None
	datebirth = None
	regno = None
	it = 0
	while it < len(texts):
		current_str = texts[it].strip()
		if 'family name' in current_str.lower():
			it += 1
			if checkAllMongolian(texts[it].lower().strip()):
				surname = texts[it].lower().strip()
			else:
				print('surname not all mongolian: ' + texts[it].lower().strip())
		elif 'эцэг/эх' in current_str.lower() or 'surname' in texts[it].lower().strip():
			it += 1
			if checkAllMongolian(texts[it].lower().strip()):
				lastname = texts[it].lower().strip()
			else:
				print('lastname not all mongolian: ' + texts[it].lower().strip())
		elif 'given name' in current_str.lower():
			it += 1
			if checkAllMongolian(texts[it].lower().strip()):
				firstname = texts[it].lower().strip()
			else:
				print('firstname not all mongolian: ' + texts[it].lower().strip())
		elif 'birth' in current_str.lower() or 'date' in current_str.lower():
			it += 1
			if checkDate(texts[it].lower().strip()):
				birthdate = texts[it].lower().strip()
			else:
				print('date is not correct: ' + texts[it].lower().strip())
		elif 'регистр' in texts[it].lower().strip() or 'registr' in texts[it].lower().strip():
			it+=1
			current_str = texts[it].lower().strip()
			regnoArr = current_str.split('/')
			for tmp in regnoArr:
				tmp = tmp.strip()
				print(tmp)
				first_two = tmp[:2].replace("0", "о").replace("x", "х")
				last_eight = tmp[2:10].replace("о", "0").replace('в', '8').replace('b','8').replace('э','3').replace('з', '3')
				print(first_two + last_eight)
				if checkRegno((first_two+last_eight).upper()):
					regno = (first_two+last_eight).upper()
					break

		it += 1
	print('Surname ' + surname if surname != None else 'NULL')
	print('Lastname ' + lastname if lastname != None else 'NULL')
	print('Firstname ' + firstname if firstname != None else 'NULL')
	print('DateOfBirth ' + birthdate if birthdate != None else 'NULL')
	print('regno ' + regno if regno != None else 'NULL')
	endtime = time.time()
	print('Duration: ' + str(endtime - starttime))

def mainBackGoogle(path):
	starttime = time.time()
	checkStringFrontMn = 'Улсын бүртгэлийн Ерөнхий газар';
	checkStringFrontEng = 'The General Authority for State Registration';
	text = detect_text(path)
	# print(text)
	texts = text.split('\n')
	if len(texts) < 5 or (texts[1].lower().strip() != checkStringFrontMn.lower() and texts[2].lower().strip() != checkStringFrontEng.lower()) :
		return 'not id card FRONT'
	issueDate = None
	expiryDate = None
	it = 0
	while it < len(texts):
		current_str = texts[it].strip()
		if 'date of issue' in current_str.lower():
			it += 1
			if checkDate(texts[it].lower().strip()):
				issueDate = texts[it].lower().strip()
			else:
				print('date is not correct: ' + texts[it].lower().strip())
		elif 'date of expiry' in current_str.lower():
			it += 1
			if checkDate(texts[it].lower().strip()):
				expiryDate = texts[it].lower().strip()
			else:
				print('date is not correct: ' + texts[it].lower().strip())
		it += 1
	print('issueDate ' + issueDate if issueDate != None else 'NULL')
	print('expiryDate ' + expiryDate if expiryDate != None else 'NULL')
	endtime = time.time()
	print('Duration: ' + str(endtime - starttime))

# mainFrontGoogle('/home/sukhee/Documents/zurag2/880b0613-67fc-4e80-a966-58c29e647d27.jpeg')
# mainFrontGoogle('/home/sukhee/Documents/zurag2/e9f46b7f-85cf-44cc-aa34-c0368844c385.jpeg')
# mainFrontGoogle('/home/sukhee/Documents/zurag2/e9f46b7f-85cf-44cc-aa34-c0368844c385-1.jpeg')
mainFrontGoogle('https://static.hipay.mn/imgc/e9f46b7f-85cf-44cc-aa34-c0368844c385.jpeg?q=1')
#mainBackGoogle('/home/sukhee/Documents/zurag2/0b057535-8ae6-4cb2-8b00-d02daa6b6b3e.jpeg')

