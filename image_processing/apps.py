from django.apps import AppConfig


class IdentificationrecognitionConfig(AppConfig):
    name = 'image_processing'
