from django.urls import path
from . import views

urlpatterns = [
    path('api/image_processing/', views.image_processing_api, name='image_processing_api'),
]
