from rest_framework import serializers

class ImageProcessingSerializer(serializers.Serializer):
    image = serializers.ImageField()
