import cv2
import numpy as np
import requests
import json

def image_processing_api(request):
    # Load image
    img = cv2.imread('a2.jpg')

    # Preprocessing the image
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    noise = cv2.medianBlur(gray, 3)
    thresh = cv2.threshold(noise, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    # Get contours
    contours, hierarchy = cv2.findContours(thresh, 1, 2)
    cnt = contours[0]

    # Get the center of the image
    M = cv2.moments(cnt)
    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])

    # Get area and perimeter of the image
    area = cv2.contourArea(cnt)
    perimeter = cv2.arcLength(cnt, True)

    # Get minimum bounding rectangle
    rect = cv2.minAreaRect(cnt)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    cv2.drawContours(img, [box], 0, (0, 0, 255), 2)

    # Get the bounding rectangle
    x, y, w, h = cv2.boundingRect(cnt)
    cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)

    # Get the minimum bounding box
    min_box = contours[0]
    for cnt in contours:
        perimeter = cv2.arcLength(cnt, True)
        if 300 <= perimeter <= 450:
            rect = cv2.minAreaRect(cnt)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            if min_box.sum() > box.sum():
                min_box = box

    # Apply perspective transform
    src = np.array([min_box[1], min_box[2], min_box[0], min_box[3]], dtype=np.float32)
    dst = np.array([[35, 35], [180, 35], [35, 120], [180, 120]], dtype=np.float32)
    M = cv2.getPerspectiveTransform(src, dst)
    transformed_img = cv2.warpPerspective(img, M, (200, 200))

    # Convert the image to a binary format
    ret, buffer = cv2.imencode('.jpg', transformed_img)
    buffer = buffer.tobytes()

    # Set up the API endpoint
    url = "http://localhost:8000/api/image_processing/"

    # Set up the payload for the API request
    payload = {
        "img": result3,
        "config": config
    }

    # Make the API request and store the response
    response = requests.post(url, data=payload)

    # Check if the request was successful
    if response.status_code == 200:
        # If the request was successful, extract the text from the image
        text = response.json()["text"]
        print(text)
    else:
        # If the request was unsuccessful, print an error message
        print("An error occurred while processing the image.")


